==========================================PARTIE 1=======================================

**// Ajout d'une carte et changement d'adressage IP**
```Bash
[risr@node1 system]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=ethernet
DEVICE=enp0s8
NAME=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.1.1.20
NETMASK=255.255.255.0
DNS1=1.1.1.1
DNS2=8.8.8.8
```
**// Changement de hostname**
```bash
[risr@localhost ~]$ sudo hostname node1.tp1.cesi
[risr@localhost ~]$ sudo nano /etc/hostname
[risr@localhost ~]$ [risr@localhost ~]$ hostname
node1.tp1.cesi
```
**// Visudo**
```bash
[risr@node1 system]$ sudo visudo /etc/sudoers
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL

## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL
```
**// Ajout utilisateur + ajout groupe**
```bash
[risr@localhost ~]$ sudo useradd zebi
[risr@localhost ~]$ passwd zebi
[risr@localhost ~]$ sudo usermod -a -G wheel zebi
```
**// Changement port SSH**
```bash
[risr@node1 ~]$ cd /etc/ssh/
[risr@node1 ssh]$ sudo nano sshd_config
[risr@node1 ssh]$ sudo systemctl restart sshd
	port = 69
tcp         LISTEN       0             128                           [::]:69                         [::]:*           users:(("sshd",pid=1597,fd=6))
```
**// Désactivation SELinux**
```bash
[risr@node1 system]$ sudo nano /etc/selinux/config
SELINUX=permissive
```
==========================================PARTIE 2===============================================

**// Installation nginx**
```bash
[risr@node1 ssh]$ sudo yum update
[risr@node1 ssh]$ dnf install nginx
```
**// Création service**
```bash
[risr@node1 ~]$ sudo python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
10.1.1.10 - - [06/Dec/2021 14:00:38] "GET / HTTP/1.1" 200 -
10.1.1.10 - - [06/Dec/2021 14:00:38] "GET /favicon.ico HTTP/1.1" 404 -
```
```bash
[risr@node1 ~]$ cd /etc/systemd/system/
[risr@node1 system]$ sudo nano issou.service
[Unit]
Description=Service ISSOU

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

[risr@node1 system]$ sudo systemctl daemon-reload
[risr@node1 system]$ sudo systemctl start issou
[risr@node1 system]$ sudo systemctl enable issou

[risr@node1 system]$ sudo systemctl status issou
● issou.service - Service ISSOU
   Loaded: loaded (/etc/systemd/system/issou.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-06 14:15:25 CET; 15s ago
 Main PID: 5788 (python3)
    Tasks: 1 (limit: 11407)
   Memory: 9.4M
   CGroup: /system.slice/issou.service
           └─5788 /usr/bin/python3 -m http.server 8888
```

```bash
[risr@node1 srv]$ sudo useradd web
[risr@node1 srv]$ sudo passwd web
[risr@node1 srv]$ mkdir /srv/web
[risr@node1 web]$ sudo nano issou.txt
[risr@node1 srv]$ sudo chown web web -R
```


```bash
[risr@node1 srv]$ cd /etc/systemd/system/

[risr@node1 system]$ sudo nano issou.service
[Unit]
Description=Service ISSOU

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/web

[Install]
WantedBy=multi-user.target
```

```bash
[risr@node1 system]$ sudo systemctl daemon-reload
[risr@node1 system]$ sudo systemctl restart issou.service
```


```bash
[risr@node1 system]$ curl http://0.0.0.0:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="issou.txt">issou.txt</a></li>
</ul>
<hr>
</body>
</html>
```

