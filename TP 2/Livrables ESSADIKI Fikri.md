**// Changement de configuration réseau 1er serveur**

```bash
[risr@web ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=ethernet
DEVICE=enp0s8
NAME=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.2.1.11
NETMASK=255.255.255.0
DNS1=1.1.1.1
DNS2=8.8.8.8
```

```bash
[risr@web ~]$ sudo hostname web.tp2.cesi
[risr@web ~]$ sudo nano /etc/hostname
web.tp2.cesi
```


**// Changement de configuration réseau 2eme serveur**

```bash
[risr@db ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=ethernet
DEVICE=enp0s8
NAME=enp0s8
BOOTPROTO=static
ONBOOT=yesS
IPADDR=10.2.1.12
NETMASK=255.255.255.0
DNS1=1.1.1.1
DNS2=8.8.8.8
```

```bash
[risr@db ~]$ sudo hostname db.tp2.cesi
[risr@db ~]$ sudo nano /etc/hostname
db.tp2.cesi
```

**// Creation de la BDD sur le 2eme serveur**
```bash
[risr@db /]$ sudo dnf update

[risr@db /]$ sudo dnf install mariadb-server

[risr@db /]$ sudo systemctl start mariadb.service
[risr@db /]$ sudo systemctl enable mariadb

[risr@db /]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[risr@db /]$ sudo firewall-cmd --reload

[risr@db /]$ sudo mysql_secure_installation

[risr@db /]$ sudo mysql -u root -p
```

```mysql 
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'Passe1?';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

**// Installation et configuration nextcloud**

```bash
[risr@web ~]$ sudo dnf provides mysql
[sudo] password for risr:
Last metadata expiration check: 0:42:47 ago on Tue 07 Dec 2021 02:22:23 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```


```bash
[risr@web ~]$ sudo dnf install mysql

[risr@web ~]$ sudo mysql -h 10.2.1.12 -P 3306 -u nextcloud -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

```bash
[risr@web ~]$ sudo dnf install httpd
[risr@web ~]$ sudo systemctl start httpd.service
[risr@web ~]$ sudo systemctl enable httpd.service
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[risr@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:30:43 CET; 19s ago
     Docs: man:httpd.service(8)
 Main PID: 2705 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 11407)
   Memory: 25.7M
   CGroup: /system.slice/httpd.service
           ├─2705 /usr/sbin/httpd -DFOREGROUND
           ├─2706 /usr/sbin/httpd -DFOREGROUND
           ├─2707 /usr/sbin/httpd -DFOREGROUND
           ├─2708 /usr/sbin/httpd -DFOREGROUND
           └─2709 /usr/sbin/httpd -DFOREGROUND

Dec 07 15:30:43 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 15:30:43 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 15:30:44 web.tp2.cesi httpd[2705]: Server configured, listening on: port 80
```

[risr@web ~]$ sudo curl 10.2.1.11
```html
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }

[...]
```



```bash
[risr@web ~]$ sudo dnf install epel-release
[risr@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[risr@web ~]$ dnf module enable php:remi-7.4
```

```bash
[risr@web conf.d]$ [risr@web conf.d]$ sudo nano vh.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

```bash
[risr@web conf.d]$ sudo mkdir /var/www/nextcloud
[risr@web conf.d]$ sudo mkdir /var/www/nextcloud/html
[risr@web conf.d]$ sudo chown -R apache /var/www/nextcloud/
```

```bash
[risr@web conf.d]$ sudo nano /etc/opt/remi/php74/php.ini
date.timezone = "Europe/Paris"
```

```bash
[risr@web html]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-23.0.0.zip
[risr@web html]$ unzip nextcloud-23.0.0.zip
[risr@web html]$ mv nextcloud /var/www/nextcloud/html/
[risr@web html]$ sudo chown -R apache /var/www/nextcloud/html/nextcloud
```

```bash
[risr@web config]$ cd /var/www/nextcloud/html/nextcloud/config/
[risr@web config]$ [risr@web config]$ sudo nano config.php
  'trusted_domains' => ['web.tp2.cesi'],
```


[risr@web config]$ curl web.tp2.cesi
```html
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }
```
**// Installation et configuration fail2ban**

```bash
[risr@web ssh]$ sudo dnf install fail2ban fail2ban-firewalld -y

[risr@web ssh]$ sudo systemctl start fail2ban
[risr@web ssh]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.

[risr@web ssh]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
[risr@web ssh]$ sudo nano /etc/fail2ban/jail.local
bantime = 1h
findtime = 1h
maxretry = 5
```

```bash
[risr@web ssh]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

[risr@web jail.d]$ sudo nano /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
bantime = 1d
maxretry = 3

[risr@web jail.d]$ [risr@web jail.d]$ sudo systemctl restart fail2ban
```

**// Configuration reseau serveur proxy**

```bash
[risr@web ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=ethernet
DEVICE=enp0s8
NAME=enp0s8
BOOTPROTO=static
ONBOOT=yes
IPADDR=10.2.1.13
NETMASK=255.255.255.0
DNS1=1.1.1.1
DNS2=8.8.8.8
```

```bash
[risr@web ~]$ sudo hostname proxy.tp2.cesi
[risr@web ~]$ sudo nano /etc/hostname
proxy.tp2.cesi
```

**// Configuration service proxy et TLS**

```bash
[risr@proxy nginx]$ sudo nano nginx.conf
    server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;
        server_name  web.tp2.cesi;

        ssl_certificate "/etc/pki/nginx/server.crt";
        ssl_certificate_key "/etc/pki/nginx/private/server.key";
        ssl_session_cache shared:SSL:1m;
        ssl_session_timeout  10m;
        ssl_ciphers PROFILE=SYSTEM;
        ssl_prefer_server_ciphers on;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        proxy_pass http://web.tp2.cesi/nextcloud/;
        }

     }

}
```


**// Redirection HTTP > HTTPS**

```bash
[risr@proxy nginx]$ [risr@proxy nginx]$ sudo nano conf.d/redirect.conf
server {
    listen 80;
    server_name  web.tp2.cesi;
    return 301 https://web.tp2.cesi$request_uri;
}
```

**// Installation et configuration NetDATA**

```bash
[risr@web /]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
--- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-WtwaJJ860m]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-WtwaJJ860m/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK

[/tmp/netdata-kickstart-WtwaJJ860m]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-WtwaJJ860m/netdata-latest.gz.run https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run
 OK

 --- Installing netdata ---
[/tmp/netdata-kickstart-WtwaJJ860m]$ sudo sh /tmp/netdata-kickstart-WtwaJJ860m/netdata-latest.gz.run -- --auto-update
```

```bash
[risr@web /]$ sudo firewall-cmd --add-port=19999/tcp --permanent
[risr@web /]$ sudo firewall-cmd --reload

[risr@web /]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 69/tcp 80/tcp 8888/tcp 19999/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


[risr@web /]$ curl 10.2.1.11:19999
```html
<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name" content="netdata"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="viewport" content="width=device-width,initial-scale=1"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><meta name="author" content="costa@tsaousis.gr"><link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAP9JREFUeNpiYBgFo+A/w34gpiZ8DzWzAYgNiHGAA5UdgA73g+2gcyhgg/0DGQoweB6IBQYyFCCOGOBQwBMd/xnW09ERDtgcoEBHB+zHFQrz6egIBUasocDAcJ9OxWAhE4YQI8MDILmATg7wZ8QRDfQKhQf4Cie6pAVGPA4AhQKo0BCgZRAw4ZSBpIWJNI6CD4wEKikBaFqgVSgcYMIrzcjwgcahcIGRiPYCLUPBkNhWUwP9akVcoQBpatG4MsLviAIqWj6f3Absfdq2igg7IIEKDVQKEzN5ofAenJCp1I8gJRTug5tfkGIdR1FDniMI+QZUjF8Amn5htOdHCAAEGACE6B0cS6mrEwAAAABJRU5ErkJggg=="/><meta property="og:locale" content="en_US"/><meta property="og:url" content="https://my-netdata.io"/><meta property="og:type" content="website"/><meta property="og:site_name" content="netdata"/><meta property="og:title" content="Get control of your Linux Servers. Simple. Effective. Awesome."/><meta property="og:description" content="Unparalleled insights, in real-time, of everything happening on your Linux systems and applications, with stunning, interactive web dashboards and powerful performance and health alarms."/><meta property="og:image" content="https://cloud.githubusercontent.com/assets/2662304/22945737/e98cd0c6-f2fd-11e6-96f1-5501934b0955.png"/><meta property="og:image:type" content="image/png"/><meta property="fb:app_id" content="1200089276712916"/><meta name="twitter:card" content="summary"/><meta name="twitter:site" content="@linuxnetdata"/><meta name="twitter:title" content="Get control of your Linux Servers. Simple. Effective. Awesome."/><meta name="twitter:description" content="Unparalleled insights, in real-time, of everything happening on your Linux systems and applications, with stunning, interactive web dashboards and powerful performance and health alarms."/><meta name="twitter:image" content="https://cloud.githubusercontent.com/assets/2662304/14092712/93b039ea-f551-11e5-822c-beadbf2b2a2e.gif"/><style>.loadOverlay{position:fixed;top:0;left:0;width:100%;height:100%;z-index:2000;font-size:10vh;font-family:sans-serif;padding:40vh 0 40vh 0;font-weight:700;text-align:center}</style><link href="./static/css/2.20fd0a40.chunk.css" rel="stylesheet"><link href="./static/css/main.a46a34fa.chunk.css" rel="stylesheet"></head><body data-spy="scroll" data-target="#sidebar" data-offset="100"><div id="loadOverlay" class="loadOverlay" style="background-color:#fff;color:#888"><div style="font-size:3vh">You must enable JavaScript in order to use Netdata!<br/>You can do this in <a href="https://enable-javascript.com/" target="_blank">your browser settings</a>.</div></div><script type="text/javascript">// Cleanup JS warning.
[...]
```
**// Installation alerting discord**

```bash
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/918092449912602655/E47vK3jbGyhIVvUlVB4-lkeEEVEtj6gP0S7dmXxh28pQd2kzGav3QydNbaxQCTFkNrfG"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```


**// Installation et configuration de la solution de backup**

```bash
[risr@web /]$ cd
[risr@web /]$ curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
[risr@web /]$ sudo cp borg-linux64 /usr/local/bin/borg
[risr@web /]$ sudo chown root:root /usr/local/bin/borg
[risr@web /]$ sudo chmod 755 /usr/local/bin/borg

[risr@web system]$ borg init --encryption=repokey /backup
```

```bash
[risr@web system]$ cd /etc/systemd/system

[risr@web system]$ sudo nano backup_db.service
[Unit]
Description=Service de Backup ISSOU

[Service]
ExecStart=borg create --stats /backup::nextcloud_YYMMDD_HHMMSS /var/www/httpd/nextcloud
Type=oneshot

[Install]
WantedBy=multi-user.target
```

```bash
[risr@web system]$ sudo nano backup.timer
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup_db.service

[Timer]
Unit=backup_db.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

```bash
[risr@web system]$ sudo systemctl daemon-reload
[risr@web system]$ sudo systemctl start backup.timer
[risr@web system]$ sudo systemctl enable backup.timer

[risr@web system]$ sudo systemctl list-timers
NEXT                         LEFT         LAST                         PASSED       UNIT                         ACT>
Wed 2021-12-08 17:00:00 CET  37min left   n/a                          n/a          backup.timer                 bac>
Wed 2021-12-08 17:25:13 CET  1h 2min left Wed 2021-12-08 16:20:59 CET  1min 30s ago dnf-makecache.timer          dnf>
Thu 2021-12-09 00:00:00 CET  7h left      Wed 2021-12-08 08:57:34 CET  7h ago       mlocate-updatedb.timer       mlo>
Thu 2021-12-09 16:09:14 CET  23h left     Wed 2021-12-08 16:09:14 CET  13min ago    systemd-tmpfiles-clean.timer sys>

4 timers listed.
```

